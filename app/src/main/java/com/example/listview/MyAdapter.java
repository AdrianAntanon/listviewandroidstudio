package com.example.listview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<String> names;

    public MyAdapter(Context context, int layout, List<String> names) {
        this.context = context;
        this.layout = layout;
        this.names = names;
    }

    @Override
    public int getCount() {
        return this.names.size();
    }

    @Override
    public Object getItem(int position) {
        return this.names.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {

//        Inflem la vista que ens ha arribat amb el nostre layout personalitzat
            LayoutInflater inflater = LayoutInflater.from(this.context);
            convertView = inflater.inflate(this.layout, null);

            holder = new ViewHolder();

            holder.nameTextView = convertView.findViewById(R.id.textView);

            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }

//        Portem el valor actual depenent de la posició
        String currentName = (String) getItem(position);

//        Referenciem l'element a modificar i l'omplim
        holder.nameTextView.setText(currentName);

        return convertView;
    }

    static class ViewHolder {
        private TextView nameTextView;
    }
}
